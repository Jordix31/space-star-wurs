﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	private void Start(){
		Time.timeScale = 1;
	}

	public void Exit()
    {
        Debug.Log("Exit");
        Application.Quit();
    }

    public void Play()
    {
        Debug.Log("Play");
        SceneManager.LoadScene("SpaceShooter");
    }
}
