﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	public GameObject SmallEnemy;
	float maxSpawnRateInSeconds = 5f;

	// Use this for initialization
	void Start () {
		Invoke ("SpawnEnemy", maxSpawnRateInSeconds);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SpawnEnemy()
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
		GameObject anEnemy = (GameObject)Instantiate (SmallEnemy);
		anEnemy.transform.position = new Vector2 (Random.Range (min.x, max.x), max.y);
		ScheduleNextEnemySpawn ();
	}
	void ScheduleNextEnemySpawn()
	{
		float spawnInNSeconds;
		if (maxSpawnRateInSeconds > 1f) {
			spawnInNSeconds = Random.Range (0.1f, maxSpawnRateInSeconds);
		} else
			spawnInNSeconds = 1f;
		Invoke ("SpawnEnemy", spawnInNSeconds);
	}


}
