﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasicEnemy : MonoBehaviour {

	protected bool launched;
	protected Vector2 moveSpeed;
	public ParticleSystem particle;
	public AudioSource audioExplosion;

	private Transform graphics;
	private Collider2D basicenemyCollider;


	private Vector3 iniPos;

	void Start(){
		iniPos = transform.position;
		graphics = transform.GetChild (0);
		basicenemyCollider = GetComponent<Collider2D> ();
	}

	public void LaunchBasicEnemy (Vector3 position,Vector2 direction, float rotation){
		transform.position = position;
		launched = true;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		moveSpeed = direction;
		graphics.gameObject.SetActive (true);
		basicenemyCollider.enabled = true;
	}

	// Update is called once per frame
	protected void Update () {
		if (launched) {
			transform.Translate (moveSpeed.x*Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
		}
	}

	protected void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet") {
			Explode ();
		}else if(other.tag == "Finish") {
			Reset ();
		}
	}

	protected void Reset(){
		transform.position = iniPos;
		launched = false;
	}

	protected virtual void Explode(){
		audioExplosion.Play ();
		basicenemyCollider.enabled = false;
		launched = false;
		particle.Play ();
		graphics.gameObject.SetActive (false);
		Invoke ("Reset", 1);
	}
}
