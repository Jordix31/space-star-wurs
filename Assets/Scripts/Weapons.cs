﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
	private Cannon cannon;
	private Cannon2 cannon2;
	public int maxAmmo;

	public Transform ammoTransfom;
	//TESTING VARIABLES
	public Cartridge[] cartridges;
	public int currentCartridge = 0;
	public GameObject[] bulletsResources;

	private void Start()
	{
		bulletsResources = Resources.LoadAll<GameObject>("Prefabs/Bullets");

		cartridges = new Cartridge[bulletsResources.Length];

		Vector2 spawnPos = ammoTransfom.position;

		for(int i = 0; i < bulletsResources.Length; i++)
		{
			GameObject obj = new GameObject();
			obj.name = "Cartridge_" + i;
			obj.transform.parent = ammoTransfom;
			obj.transform.localPosition = Vector3.zero;

			cartridges[i] = new Cartridge(bulletsResources[i], obj.transform, spawnPos,  maxAmmo);
			spawnPos.y -= 1;
		}

		cannon = GetComponentInChildren<Cannon>();
		cannon2 = GetComponentInChildren<Cannon2>();

	}

	public void ShotWeapon()
	{
		cannon.ShotCannon(cartridges[currentCartridge]);
		cannon2.ShotCannon(cartridges[currentCartridge]);
	}
	public void NextCartridge()
	{
		currentCartridge++;
		if(currentCartridge >= cartridges.Length)
		{
			currentCartridge = 0;
		}
	}
	public void PreviousWeapon()
	{
		currentCartridge--;
		if(currentCartridge < 0 )
		{
			currentCartridge = cartridges.Length - 1;
		}
	}
}
