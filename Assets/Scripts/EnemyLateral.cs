﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLateral : MonoBehaviour {
	float speed;
	public ParticleSystem particle;
	public AudioSource audioExplosion;

	SpriteRenderer spritevar;
	BoxCollider2D boxcoll;

	void Start () {
		MyGameManager score = GetComponent<MyGameManager> ();
		speed = 2f;
	    spritevar = GetComponent<SpriteRenderer> ();
	    boxcoll = GetComponent<BoxCollider2D> ();
    }

	void Update ()
	{
		transform.Translate(Vector2.left * speed * Time.deltaTime);

	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Bullet" || other.tag == "Player"){
			Explota();
		}
	}

	void Explota()
	{

		MyGameManager.getInstance().AddHighscore(50);
		audioExplosion.Play ();
		particle.Play ();
		Invoke ("Reset", 1);
		spritevar.enabled = false;
		boxcoll.enabled = false;

	}
}
