﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour {
	public Text highscoreText;
	public Text livesText;
	private int lives;
	public int highscore;
	public SpawnerLateral spawner;

    public GameObject pauseMenu;

	private static MyGameManager instance;

	void Awake(){
		if (instance == null) {
			instance = this;
		}
	}

	public static MyGameManager getInstance(){
		return instance;
	}

	// Use this for initialization
	void Start () {
		lives = 3;
		highscore = 0;
		highscoreText.text = highscore.ToString("D5");
		//livesText.text = "x3";
		livesText.text = "x " + lives.ToString ();
		spawner = GameObject.FindGameObjectWithTag("SpawnerLateral").GetComponent<SpawnerLateral>();
		spawner.Initialize();
	}
	
	public void LoseLive(){
		lives--;
		livesText.text = "x " + lives.ToString ();
		if (lives < 0) {
			SceneManager.LoadScene("GameOver");
		}
	}

	public void AddHighscore(int value){
		highscore += value;
		highscoreText.text = "x" + highscore.ToString ();
	}

    public void Pause()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }



    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
		spawner.MyUpdate(); 
    }
}
